import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Warship {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[][] board = new int[6][6];
        for (int i = 0; i < board.length; i++) {
            board[0][i] = i;
            board[i][0] = i;
        }
        Random random = new Random();
        int column = random.nextInt(1,6);
        int row = random.nextInt(1,6);
        System.out.println(column + " " + row);

        while (true) {
            try {
                System.out.println("Guess the column:");
                int guesscolumn= sc.nextInt();
                checker(guesscolumn);
                System.out.println("Guess the row:");
                int guessrow = sc.nextInt();
                checker(guessrow);
                if (guessrow == row && guesscolumn == column ) {
                    board[guesscolumn][guessrow] = 1;
                    System.out.println("You are winner");
                    showTheBoard(board);
                    break;
                } else {

                    board[guesscolumn][guessrow] = -1;
                    showTheBoard(board);
                }

            } catch (InputMismatchException exception){
                System.out.println("Enter number,please");
                sc.next();

            }
            catch (IndexOutOfBoundsException exception){
                System.out.println("Duz ededd gir");
            }

        }
    }

    public static void checker(int inputDigit) {
        if (inputDigit <= 0 || inputDigit > 5) {
            throw new java.lang.IndexOutOfBoundsException("Index Out of bound") ;
        }
    }
    public static void showTheBoard(int[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == 1 && i != 0 && j != 0) {
                    System.out.print("x" + "|");

                } else if (board[i][j] == -1) {
                    System.out.print("*" + "|");

                } else {
                    if (board[i][j] == 0 && i != 0 && j != 0) {
                        System.out.print("-" + "|");
                    }else{
                        System.out.print(board[i][j] + "|");
                    }}
                }
                System.out.println();

            }
        }


    }
